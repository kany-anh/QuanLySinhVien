function layThongTinTuFrom() {
  var _maSV = document.getElementById("txtMaSV").value;
  var _tenSv = document.getElementById("txtTenSV").value;
  var _email = document.getElementById("txtEmail").value;
  var _pass = document.getElementById("txtPass").value;
  var _diemLy = document.getElementById("txtDiemLy").value * 1;
  var _diemHoa = document.getElementById("txtDiemHoa").value * 1;
  var _diemToan = document.getElementById("txtDiemToan").value * 1;

  console.log(_maSV, _tenSv, _email, _pass, _diemLy, _diemHoa, _diemToan);
  return new SinhVien(
    _maSV,
    _tenSv,
    _email,
    _pass,
    _diemLy,
    _diemHoa,
    _diemToan
  );
}

function renderDSSV(svArr) {
  var contentHTML = "";
  for (var index = 0; index < svArr.length; index++) {
    var sv = svArr[index];
    contentHTML += ` <tr>
                    <td>${sv.maSV}</td>
                    <td>${sv.tenSv}</td>
                    <td>${sv.email}</td>
                    <td>${sv.tinhDTB()}</td>
                    <td>
                    <button onclick="xoaSV('${
                      sv.maSV
                    }')" class="btn btn-danger">Xoá</button>
                    <button onclick="suaSV('${
                      sv.maSV
                    }')" class="btn btn-warning">Sửa</button>
                    </td>
                    </tr> `;
  }
  document.getElementById("tbodySinhVien").innerHTML = contentHTML;
}

function timKiemViTri(id, arr) {
  var viTri = -1;
  for (var index = 0; index < arr.length; index++) {
    if (arr[index].maSV == id) {
      viTri = index;
    }
  }
  return viTri;
}
