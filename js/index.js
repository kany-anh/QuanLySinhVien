var DSSV = [];

let dataJson = localStorage.getItem("DSSV_LOCAL");

if (dataJson != null) {
  var dataArr = JSON.parse(dataJson);
  DSSV = dataArr.map(function (item) {
    var sv = new SinhVien(
      item.maSV,
      item.tenSv,
      item.email,
      item.pass,
      item.diemToan,
      item.diemLy,
      item.diemHoa
    );
    return sv;
  });
  renderDSSV(DSSV);
}

function themSV() {
  var sv = layThongTinTuFrom();
  var isValid = true;
  isValid =
    kiemTraSo(sv.maSV) &&
    kiemTraTrung(sv.maSV, DSSV) &&
    kiemTraDoDai(sv.maSV, "spanMaSV", 6, 8);
  isValid = isValid & kiemTraEmail(sv.email);

  console.log(isValid);
  if (isValid) {
    DSSV.push(sv);
    console.log(DSSV);
    var dssvJson = JSON.stringify(DSSV);
    localStorage.setItem("DSSV_LOCAL", dssvJson);
    renderDSSV(DSSV);
  }
}

function xoaSV(idSV) {
  var viTri = timKiemViTri(idSV, DSSV);
  console.log(viTri);
  if (viTri != -1) {
    DSSV.splice(viTri, 1);
    renderDSSV(DSSV);
  }
}

function suaSV(idSV) {
  var viTri = timKiemViTri(idSV, DSSV);
  if (viTri != -1) {
    var sv = DSSV[viTri];
    document.getElementById("txtTenSV").value = sv.tenSv;
    document.getElementById("txtEmail").value = sv.email;
    document.getElementById("txtPass").value = sv.pass;
    document.getElementById("txtDiemHoa").value = sv.diemHoa;
    document.getElementById("txtDiemToan").value = sv.diemToan;
    document.getElementById("txtDiemLy").value = sv.diemLy;

    document.getElementById("txtMaSV").disabled = true;
  }
}
