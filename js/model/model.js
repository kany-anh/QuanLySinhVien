function SinhVien(_maSV, _tenSv, _email, _pass, _diemLy, _diemHoa, _diemToan) {
  this.maSV = _maSV;
  this.tenSv = _tenSv;
  this.email = _email;
  this.pass = _pass;
  this.diemLy = _diemLy;
  this.diemHoa = _diemHoa;
  this.diemToan = _diemToan;
  this.tinhDTB = function () {
    return ((this.diemHoa + this.diemLy + this.diemToan) / 3).toFixed(2);
  };
}
